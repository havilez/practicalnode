"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const glob_1 = require("glob");
const lodash_1 = require("lodash");
class Config {
    static globFiles(location) {
        return lodash_1.union([], glob_1.sync(location));
    }
}
Config.port = 3000;
// note its retriving routes and models from exported object in javaScript files
// will this work when debugging typeScript?????
Config.routes = './lib/routes/**/*.js';
Config.models = './dist/models/**/*.js';
Config.useMongo = false;
Config.mongodb = process.env.NODE_ENV === 'docker' ?
    'mongodb://mongo:27017/express-typescript-starter' :
    'mongodb://localhost:27017/express-typescript-starter';
exports.default = Config;
//# sourceMappingURL=config.js.map