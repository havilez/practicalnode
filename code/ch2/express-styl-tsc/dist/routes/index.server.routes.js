"use strict";
// import * as express from 'express';
// let router = express.Router()
Object.defineProperty(exports, "__esModule", { value: true });
const index_server_controller_1 = require("../controllers/index.server.controller");
class IndexRoute {
    constructor(app) {
        app.route('/')
            .get(index_server_controller_1.indexController.index);
        app.route('/msg')
            .get(index_server_controller_1.indexController.msg);
    }
}
exports.default = IndexRoute;
//# sourceMappingURL=index.server.routes.js.map