import { sync } from 'glob';
import { union } from 'lodash';

export default class Config {
    public static port: number = 3000;
    // note its retriving routes and models from exported object in javaScript files
    // will this work when debugging typeScript?????
	public static routes: string = './lib/routes/**/*.js';
	public static models: string = './dist/models/**/*.js';
	public static useMongo: boolean = false;
	public static mongodb = process.env.NODE_ENV === 'docker' ? 
	'mongodb://mongo:27017/express-typescript-starter' : 
	'mongodb://localhost:27017/express-typescript-starter';
	public static globFiles(location: string): string[] {
		return union([], sync(location));
	}
}